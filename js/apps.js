﻿function iniciar() {
    /* Fecha y hora */
    empezarHora();

    /* Menu desplegable */
    menuPadre = document.getElementById("menu-desplegable");
    menuPadre.addEventListener("mouseover", menuDesplegable, false);
    menuPadre.addEventListener("mouseout", menuDesplegable, false);

    /* Cambiar fondo */
    document.body.addEventListener("keydown", cambiaFondo, false);

    /* Tooltip para links (por ejemplo), mostrando su title */
    try {
        var link_udemy = document.getElementById("udemy");
        var link_tutellus = document.getElementById("tutellus");
        link_udemy.addEventListener("mouseover", toolTip, false);
        link_udemy.addEventListener("mouseout", toolTip, false);
        link_tutellus.addEventListener("mouseover", toolTip, false);
        link_tutellus.addEventListener("mouseout", toolTip, false);
    } catch (error) {
        console.log("No se han encontrado los links udemy ni tutellus: " + error.message);
    }

    /* Ventana modal */
    crearVentanaModal();
}

/* Añade un cero a los elementos de la fecha y hora
que son menores de 10 */
function addCero(part) {
    if (part < 10) {
        part = "0" + part;
    }
    return part;
}

function actualizaHora() {
    var fecha = new Date();
    var dia = addCero(fecha.getDate());
    var mes = addCero(fecha.getMonth()+1);
    var an = fecha.getFullYear();
    var horas = addCero(fecha.getHours());
    var minutos = addCero(fecha.getMinutes());
    var segundos = addCero(fecha.getSeconds());
    document.getElementById("barra-hora").innerHTML = dia + "/" + mes + "/" + an + " - " + horas + ":" + minutos + ":" + segundos;
}

/* Comienza a mostrar la fecha y la hora */
function empezarHora() {
    setInterval(actualizaHora, 1000);
}

/* Mostrar/Ocultar menú desplegable */
function menuDesplegable(ev) {
    var miEvento = ev || window.event;
    var submenu = document.getElementById("submenu");
    if (miEvento.type == "mouseover") {
        submenu.style.display = 'block';
    } else if (miEvento.type == "mouseout") {
        submenu.style.display = 'none';
    }
}

function generarColorAleatorio() {
    var numHexadecimal = new Array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
    var color = '#';
    for (var i=0;i<6;i++) {
        var pos = Math.random() * 15;
        pos = parseInt(Math.round(pos));
        color = color + numHexadecimal[pos];
    }
    return color;
}

/* Cambia el fondo con un color aleatorio al pulsar ctrl + c */
function cambiaFondo(miEvento) {
    var ev = miEvento || window.event;
    var codigo = ev.keyCode;
    var color = generarColorAleatorio();
    /* Ctrl + c */
    if (codigo == 67 && ev.ctrlKey) {
        document.getElementById("contenido").style.background = color;
        alert('Se se cambiará el color de condo con: ' + color);
    }
}

/* Función para crear un pequeño bocadillo en la posición 
donde se encuentra el ratón para añadir información*/
function toolTip(elEvento) {
    var miEvento = elEvento || windo.event;
    if (miEvento.type == "mouseout") {
        miEvento.target.parentElement.removeChild(
            document.getElementById("tooltip")
            );
    } else if (miEvento.type == "mouseover") {
        var posY = miEvento.pageY -55;
        var posX = miEvento.pageX -40;
        var contenedor = document.createElement("div");
        contenedor.id = 'tooltip';
        contenedor.style.position = 'absolute';
        contenedor.style.left = posX + 'px';
        contenedor.style.top = posY + 'px';
        contenedor.style.background = '#eee';
        contenedor.style.border = '1px solid #ccc';
        contenedor.style.padding = '10px';
        contenedor.innerHTML = miEvento.target.title;
        miEvento.target.parentElement.appendChild(contenedor);
    }
}

/* Elimina la ventana modal si está activada */
function eliminarVentanaModal() {
    var ventana = document.getElementById("ventana-modal");
    if (ventana != null) {
        document.body.removeChild(ventana);
    }
}

/* Actualiza la posición vertical de la ventana si está activada */
function actualizaPosVentanaModal() {
    var ventana = document.getElementById("ventana-modal");
    if (ventana != null) {
        ventana.style.top = (document.body.scrollTop+(window.innerHeight * 0.3)) + "px";
    }
}

/* Crea la ventana modal cuando se llama la función */
function crearVentanaModal() {
    var miVentana = document.createElement("div");
    miVentana.id = "ventana-modal";
    miVentana.style.background = "rgba(0,0,0,0.6)";
    miVentana.style.color = "#fff";
    miVentana.style.position = "absolute";
    /* Quiero que mida el 80% de la ventana visible */
    miVentana.style.width = (window.innerWidth * 0.8) + "px";
    miVentana.style.top = (window.innerHeight * 0.3) + "px";
    miVentana.style.left = (window.innerWidth * 0.1) + "px";
    miVentana.style.zIndex = '2';
    miVentana.style.padding = "20px";

    /* Texto de la ventana */
    var p = document.createElement("p");
    p.appendChild(document.createTextNode("Bienvenid@, esta es la web de Carl Johnson, aquí puedes encontrar todo lo relacionado con el escritor y sus libros. Esta es la ventana modal. El tool tipo se puede observar el nos enlaces de Udemy y Tutellus. El menú desplegable se ha aplicado al ítem Libros del menú. Por otro lado, si se hace scroll se puede observar como la ventana sigue en pantalla. Un poquito más abajo se puede observar el banner en accion, vá cambiando de imagen y texto cada 3 segundos y te redirige al hacer click"));
    
    /* Cerrar ventana modal */
    var p2 = document.createElement("p");
    var cerrar = document.createElement("button");
    cerrar.id = "v-m-cerrar";
    cerrar.textContent = ' Cerrar ';
    p2.appendChild(cerrar);
   
    miVentana.appendChild(p);
    miVentana.appendChild(p2);
    document.body.appendChild(miVentana);

    /* Cerrar la ventana */
    cerrar.addEventListener("click", eliminarVentanaModal, false);
    /* Actualizar la posición de la ventana modal */
    document.addEventListener("scroll", actualizaPosVentanaModal, false);
}

window.addEventListener("load", iniciar, false);