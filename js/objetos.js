﻿function iniciarObjetos() {
    document.getElementById("interes").addEventListener('change', actualizaIndicadorInteres, false);
    document.getElementById("add-tematica").addEventListener('click', addTematica, false);
    /* Array de los objetos que salgan del formulario */
    lasTematicas = new Array();

}

/* Actualizar el indicador del interés */
function actualizaIndicadorInteres(e) {
    var miEvento = e || window.event;
    var num = e.target.value;
    var indicador = e.target.nextSibling;
    indicador.textContent = num + " sobre 10";
}

/* Clase temática */
function tematica(nombre, interes) {
    this.nombre = nombre;
    this.interes = interes;
}

/* Función que construye una temática indicada por el usuario */
function funcionConstructora(nombre, interes) {
    miTematica = new tematica(nombre, interes);
    lasTematicas.push(miTematica);
}

function validaNombre(nom) {
    var estado = true;
    if (nom.length >= 3 && nom.length <= 30) {
        estado = true;
    } else {
        estado = false;
        alert('La temática debe tener entre 3 y 30 caracteres');
    }

    for (var i = 0; i < lasTematicas.length; i++) {
        if (lasTematicas[i].nombre == nom) {
            alert('El nombre ya existe, si quieres cambiar el interés, pincha en modificar');
            estado = false;
            break;
        } 
    }
    return estado;
}

function checkModificacion(e) {
    var miEvento = e || window.event;
    nombreAnterior = miEvento.target.parentElement.parentElement.id;
    var nuevoNombre = document.getElementById('modify-name').value;
    var nuevoInteres = document.getElementById('modify-interes').value;

    if (nuevoNombre.length>=3 && nuevoNombre.length<=30) {
        for (var i = 0; i < lasTematicas.length; i++) {
            if (lasTematicas[i].nombre == nombreAnterior) {
                if (confirm('¿Seguro que quieres cambiar la temática a: ' + nuevoNombre + ' y el interés a: ' + nuevoInteres +'?') == true) {
                    lasTematicas[i].nombre = nuevoNombre;
                    lasTematicas[i].interes = nuevoInteres;
                    listarTematicas();
                    /* Actualizamos las modificaciones */
                }
                break;
            }
        }
    }
}

/* Modificar temática */
function modifyTematica(e) {
    var miEvento = e || window.event;
    var padre = miEvento.target.parentElement;
    laTematica = null;
    var nombreTematica = miEvento.target.id.substr(10);
    for (var i = 0; i < lasTematicas.length; i++) {
        if (lasTematicas[i].nombre == nombreTematica) {
            laTematica = lasTematicas[i];
        }
    }
    
    var nombre_anterior = null;
    var interes_anterior = null;
   
    nombre_anterior = document.getElementById('modify-name');
    interes_anterior = document.getElementById('modify-interes');

    /* Esta función elimina el formulario anterior en caso de que exista */
    if (nombre_anterior != null && interes_anterior != null) {
        /* Si ya existen los elementos, los borramos */
        var elDiv = document.getElementById("opciones-modificar");
        var padreDelDiv = elDiv.parentElement;
        padreDelDiv.removeChild(elDiv);
    }
    var contenedor = document.createElement("div");
    contenedor.id = 'opciones-modificar';

    var inputNombre = document.createElement('input');
    inputNombre.type = 'text';
    inputNombre.name = 'modify-name';
    inputNombre.id = 'modify-name';
    inputNombre.value = laTematica.nombre;

    var inputInteres = document.createElement('input');
    inputInteres.type = 'range';
    inputInteres.min = '1';
    inputInteres.max = '10';
    inputInteres.name = 'modify-interes';
    inputInteres.id = 'modify-interes';
    inputInteres.value = laTematica.interes;
    inputInteres.addEventListener('change', actualizaIndicadorInteres, false);

    var indicador = document.createElement('span');
    indicador.id = 'indicador-interes-mod';
    indicador.textContent = laTematica.interes + ' sobre 10';

    var br = document.createElement('br');
    var br2 = document.createElement('br');
    var br3 = document.createElement('br');

    var button = document.createElement('input');
    button.type = 'button';
    button.value = 'Aplicar';
    button.id = 'aplicar';
    button.addEventListener('click', checkModificacion, false);
    
    
    contenedor.appendChild(br);
    contenedor.appendChild(inputNombre);
    contenedor.appendChild(br2);
    contenedor.appendChild(inputInteres);
    contenedor.appendChild(indicador);
    contenedor.appendChild(br3);
    contenedor.appendChild(button);
    padre.appendChild(contenedor);
}

/* Eliminar temática */
function deleteTematica(e) {
    var miEvento = e || window.event;
    if (confirm('Aceptar para confirmar la eliminación; Cancelar en caso contrario') == true) {
        for (var i = 0; i < lasTematicas.length; i++) {
            var nom = "eliminar-" + lasTematicas[i].nombre;
            if (nom == miEvento.target.id) {
                lasTematicas.splice(i, 1);/* Liminamos del array */
                /* Actualizamos las temáticas*/
                listarTematicas();
                break;
            }
        }
    }
}

/* Añadir temática */
function addTematica() {
    var nom = document.getElementById("tematica").value;
    var interes = document.getElementById("interes").value;
    if (validaNombre(nom) == true) {
        funcionConstructora(nom, interes);
        listarTematicas();
    }
}

/* Elimina los hijos, para poder actualizar correctamente lo que observarmos */
function eliminarHijos() {
    var ventana = document.getElementById("opciones_added");
    if (ventana.hasChildNodes() == true) {
        while (ventana.childNodes.length>=1) {
            ventana.removeChild(ventana.lastChild);   
        }
    }
}

/* Función que muestra todas las temáticas añadidas */
function listarTematicas() {
    eliminarHijos();
    var ventana = document.getElementById("opciones_added");
    for (var i = 0; i < lasTematicas.length; i++) {
        var p = document.createElement("p");
        p.id = lasTematicas[i].nombre;
        var texto = document.createTextNode("Nombre: " + lasTematicas[i].nombre + " | Interés: " + lasTematicas[i].interes + " | ");
        p.appendChild(texto);

        /* Modificar */
        var a = document.createElement("span");
        a.id = "modificar-" + lasTematicas[i].nombre;
        a.textContent = ' Modificar ';
        a.style.cursor = 'Pointer';
        a.addEventListener('click', modifyTematica, false);

        /* Eliminar */
        var a2 = document.createElement("span");
        a2.id = "eliminar-" + lasTematicas[i].nombre;
        a2.textContent = ' Eliminar ';
        a2.style.cursor = 'Pointer';
        a2.addEventListener('click', deleteTematica, false);

        p.appendChild(a);
        p.appendChild(document.createTextNode(" | "));
        p.appendChild(a2);
        ventana.appendChild(p);
    }
}

window.addEventListener("load", iniciarObjetos, false);