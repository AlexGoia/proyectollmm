﻿function iniciarElemForms() {
    /* He añadido este elemento que estaba en la parte apps, aquí.
    Estructura con dos selects. Elegir en el primero modifica el segundo. 
    Elegir al segundo modifica el contenido de algún elemento de la página (imagen por ejemplo). 
    */
    selectPadre = document.getElementById("tematica");
    selectPadre.addEventListener("change", selectDinamico, false);

    /* Validación del formulario */
    /* He creado un formulario de suscripción en index.html para este fin */   
    document.getElementById("enviar-suscripcion").addEventListener("click", validar, false);

    /* Limitar textarea */
    textarea = document.getElementById("sobre-suscriptor");
    textarea.addEventListener("keypress", limitaTextArea, false);// no se por qué no lo hace
    document.getElementById("reset-sobre-suscriptor").addEventListener("click", resetTextArea, false);

    /* Checkbox */
    checkBox = document.getElementById("sms");
    checkBox.addEventListener("change", enablePhone, false);

}

function validar(ev) {
    var evento = ev || window.ev;
    var correcto = true;
    correcto = validarNombre();
    correcto = validarEmail();
    correcto = validarTextarea();
    correcto = validarSelects();
    
    if (checkBox.checked == true) {
        correcto = validarTelefono();
    }

    if (correcto==false) {
        alert('No se enviará el formulario');
    } else {
        ev.target.disabled = true; /* Evitamos envío dublicado */
        ev.target.value = 'Enviando...';
        ev.target.form.submit();/* Enviamos formulario */
    }
}

function validarNombre() {
    var nombre = document.getElementById("nom-suscriptor").value;
    if (nombre.length < 3 || nombre.length > 16) {
        alert("El nombre debe tener entre 3 y 16 caracteres ");
        return false;
    }
}

function validarEmail() {
    /* Al parecer es mejor hacerlo con el validador propio de html,
    aún así utilizaré aquí el pattern */
    var email = document.getElementById("email-suscriptor").value;
    var pattern = new RegExp("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");

    if (!pattern.test(email)) {
        alert("Email inválido");
        return false;
    }
}

function validarTextarea() {
    if (textarea.value.length>200 || textarea.value.length<10) {
        alert("Escribe entre 10 y 200 caracteres sobre tí.");
        return false;
    }
}

function validarSelects() {
    if (selectPadre.selectedIndex == 0 || selectHijo.selectedIndex == 0) {
        alert("Debes seleccionar sobre qué quieres recibir información y cada cuanto tiempo");
        return false;
    }
}

function limitaTextArea(e) {
    var letras = document.getElementById("letras-pendientes");
    letras.innerHTML = textarea.value.length + "/200";
    if (textarea.value.length >= 200) {
        e.preventDefault();
        /* Debería hacerlo, pero por algún motivo no lo hace */
    } else {
        return true;
    }
}

function resetTextArea() {
    textarea.value = "";
    limitaTextArea();
}

function validarTelefono() {
    var tel = document.getElementById("telefono").value;
    if (tel.length > 9 || tel.length < 9 || isNaN(tel) == false) {
        alert("El número de telefono debe tener 9 cifras");
        return false;
    }
}

function enablePhone() {
    if (checkBox.checked == true) {
        document.getElementById("telefono").disabled = false;
    } else {
        document.getElementById("telefono").disabled = true;
    }
}

function reiniciarSelects() {
    var tam = selectHijo.options.length;
    for (var i = 1; i < tam; i++) { 
        selectHijo.removeChild(selectHijo.options[tam - i]);
    }
}

function selectDinamico() {
    selectHijo = document.getElementById("frecuencia");
    var index = selectPadre.selectedIndex;
    if (index > 0) reiniciarSelects();
    switch (index) {
        case 0:
            break;
        case 1: // Novedades
            var option1 = document.createElement("option");
            option1.value = '1';
            option1.textContent = 'Cada día';

            var option2 = document.createElement("option");
            option2.value = '2';
            option2.textContent = 'Cada dos días';

            var option3 = document.createElement("option");
            option3.value = '3';
            option3.textContent = 'Cada tres días';

            selectHijo.appendChild(option1);
            selectHijo.appendChild(option2);
            selectHijo.appendChild(option3); 
            
            //alert('Se ha cambiado a 1');
            break;
        case 2: //Conferencias
            var option_Semana = document.createElement("option");
            option_Semana.value = "7";
            option_Semana.textContent = "Cada semana";

            var option_Dos_Semanas = document.createElement("option");
            option_Dos_Semanas.value = "14";
            option_Dos_Semanas.textContent = "Cada dos semanas";
            selectHijo.appendChild(option_Semana);
            selectHijo.appendChild(option_Dos_Semanas);
            break;
        case 3:// Libros
            var optionMes = document.createElement("option");
            optionMes.value = "30";
            optionMes.textContent = "Cada mes";

            var optionDosMeses = document.createElement("option");
            optionDosMeses.value = "60";
            optionDosMeses.textContent = "Cada dos meses";
            
            selectHijo.appendChild(optionMes);
            selectHijo.appendChild(optionDosMeses);
            break;
    }
}

window.addEventListener("load", iniciarElemForms, false);