﻿function iniciarBanner() {
    /* Banner */
    startBanner(810, document.getElementById("contenido"));
    banner(); /* Para que se muestre desde el principio */
}

/* Banner con imágenes y texto que cambia */
function banner() {
    var urlImagenes = ['img/img1.jpg', 'img/img2.jpg'];
    var texto1 = "¡Aprovecha la oferta!";
    var texto2 = "Hasta el 30 de Junio";
    var contenido = [texto1, texto2];
    if (pos < urlImagenes.length) {
        document.getElementById("div-banner-background").src = urlImagenes[pos];
        document.getElementById("div-banner-texto").innerHTML = contenido[pos];
        pos++;
    } else {
        pos = 0; /*Reiniciamos el carrusel */
    }
}

function redireccionar() {
    window.open("html/libros.html", "_self");
}

/* Esta función añade un banner al padre que deseemos, con el tamaño indicado*/
function startBanner(width, parent) {
    var divBanner = document.createElement("div");
    divBanner.id = "div-banner";
    divBanner.style.width = width + "px";
    divBanner.style.margin = "20px auto";

    var imgBanner = document.createElement("img");
    imgBanner.id = "div-banner-background";
    imgBanner.width = width;

    var textoPorEncima = document.createElement("div");
    textoPorEncima.id = "div-banner-texto";
    textoPorEncima.style.padding = '10px';
    textoPorEncima.style.zIndex = '1';
    textoPorEncima.style.fontSize = '38px';
    textoPorEncima.style.textAlign = 'center';
    textoPorEncima.style.color = 'rgba(0,0,0,0.8)';

    /* No establezco height para que se adapte automáticamente al width establecido */
    divBanner.appendChild(imgBanner);
    divBanner.appendChild(textoPorEncima);
    divBanner.onmouseover = divBanner.style.cursor = 'pointer';
    divBanner.addEventListener("click", redireccionar, false);
    parent.appendChild(divBanner);
    pos = 0;
    setInterval(banner, 3000);
}
window.addEventListener("load", iniciarBanner, false);